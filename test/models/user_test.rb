require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name test" do
    @user.name = "   "
    assert_not @user.valid?
    @user.name = "a"*51
    assert_not @user.valid?
  end

  test "email test" do
    @user.email = "   "
    assert_not @user.valid?
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
    mixed_email = "Foo@Bar.com"
  end

  test "password test" do
    @user.password = @user.password_confirmation = "  "
    assert_not @user.valid?
    @user.password = @user.password_confirmation = "a"*5
    assert_not @user.valid?
  end

  test "unique email test" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end
end
