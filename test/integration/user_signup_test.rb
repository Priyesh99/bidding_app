require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test "signup" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: {user: {name: "",
                                        email: "invalid",
                                        password: "abc",
                                        password_confirmation: "abc"}}
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
    assert_difference 'User.count', 1 do
      post users_path, params: {user: {name: "Priyesh",
                                        email: "priyeshdoshi61@gmail.com",
                                        password: "priyesh",
                                        password_confirmation: "priyesh"}}
    end
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end
